const express = require("express");
const router = express.Router();
const orderController = require('../controllers/order');
const auth =  require('../auth')

//create Order
router.post('/order', auth.verify, (req, res) => {
	const userData =  auth.decode(req.headers.authorization)

  if(userData.isAdmin === false){
	orderController.createOrder(userData, req.params, req.body).then(resultFromController=> res.send(resultFromController))
  } else {
    res.send("not auth")
  }
})

//retrieve User Order
router.get("/myOrder", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  orderController.getOrder(userData).then(resultFromController => res.send(resultFromController));
})

//retrieve All Order - Admin

router.get("/allOrder" , auth.verify, (req, res) => {
const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin === true){
  orderController.getAllOrders().then(resultFromController => res.send(resultFromController))
  } else {
    return res.send('not auth')
  }
});

//Update Qty Order
router.put('/:orderId/update', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  orderController.updateOrder(userData, req.params, req.body).then(resultFromController => res.send(resultFromController))
})

//delete order
router.delete('/:orderId/delete' , auth.verify, (req, res)=> {
  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin === false){
    orderController.deleteOrder(req.params).then(resultFromController => res.send(resultFromController));
  }else {
    return res.send('not auth')
  }
})

module.exports = router;

//archive order 
router.put('/:orderId/archive', auth.verify, (req,res) => {
  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin === false){
    orderController.archiveOrder(req.params).then(resultFromController => res.send(resultFromController))
  } else {
    return res.send('not auth')
  }
})

//remove item in cart - not working
router.delete('/:orderId/deleteItem', auth.verify, (req, res)=> {
  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin === false){
    orderController.removeCartItem(userData, req.params, req.body).then(resultFromController => res.send(resultFromController));
  }else {
    return res.send('not auth')
  }  
})

//Edit Cart item Qty
router.put('/:orderId/edit', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  console.log(userData)
   if(userData.isAdmin === false){
    orderController.editCartItem(userData, req.params, req.body).then(resultFromController => res.send(resultFromController));
  }else {
    return res.send('not auth')
  }  
})