const bcrypt = require('bcrypt');
const auth = require('../auth');
const Order = require('../models/Order')
const User = require('../models/User');
const Product = require('../models/Product');


module.exports.createOrder = async (userData, reqParams, reqBody) =>{

	let {orderId, productId, quantity, userId, totalAmount, status, subTotal, cartList} = reqBody
	//console.log(userData)
	//let {productId} = reqParams
	//console.log(productId)
	//console.log(orderId)

	quantity = Number.parseInt(reqBody.quantity)

	try{
		let order = await Order.findOne({userId: userData.id, status: true});
		//console.log(order.status)
		//console.log(order)
		//console.log(Product.findById({_id: productId}))
		//console.log(productId)
		const productDetails = await Product.findById(productId)
		//console.log("productDetails", productDetails)
		//console.log(productDetails) //result null


		if(order && order.status === true){
			//console.log(order);
			//cart exists for user
			let cartListIndex = order.cartList.findIndex(p => p.productId == productId);
			//console.log(cartListIndex)
			//product exists in the cart, update the quantity
			if (cartListIndex > -1){
				let orderItem = order.cartList[cartListIndex];
				//console.log(typeof(orderItem.quantity += quantity))
				//console.log(productDetails.price)
				//console.log(typeof(orderItem.quantity))
				orderItem.quantity += quantity
				orderItem.subTotal = orderItem.quantity * productDetails.price
				order.totalAmount = order.cartList.reduce((a, {subTotal})=> a + subTotal,0) //ADDED
				//console.log("With total amount")
				//console.log(order)
				order.cartList[cartListIndex] = orderItem;
			

				 productDetails.inStock -= 1

				 return await order.save().then(() => {
				 	//console.log(productDetails.inStock)
				 	return order
				 }).catch((err) => {
				 	return err
				 })

			} else {
				 //product does not exists in cart, add new item
				 order.cartList.push({productId, quantity, subTotal: parseInt(productDetails.price * quantity)});
				 order.totalAmount = order.cartList.reduce((a, {subTotal})=> a + subTotal,0)
				 return await order.save().then(() => {
				 	return order
				 }).catch((err) => {
				 	return err
				 })
			}

			order = await order.save().then(()=> {
				return order; 
			}).catch((err)=> {
				return err
			})
		} else {

			// console.log("New ORder")
			// console.log([{productId, quantity, subTotal}]);
			//no cart for user, create new cart
			const newOrder = await new Order({
				userId,
				cartList: [{productId, quantity, subTotal: parseInt(productDetails.price * quantity) }],
				totalAmount: productDetails.price
				
			});
			console.log(totalAmount)
			console.log(newOrder)

			return await newOrder.save();
		}


	} catch (err) {
		console.log(err)
		return err
	}


}

// Retrieve User Active Order
module.exports.getOrder = (userData) => {

	return Order.find({userId: userData.id, status: true}).populate({path: 'cartList', populate: 'productId'}).then(result => {

		return result
		console.log(result)
	}).catch(() => {
		return false
	})
}

//Retrieve All Checked out Order -Admin
module.exports.getAllOrders = () => {
	return Order.find({status: false}).populate({path: 'cartList', populate: 'productId'}).sort({purchasedOn: -1}).then(result => {
		return result;
	})
}
 

//delete Order
module.exports.deleteOrder = (reqParams) => {

	return Order.findByIdAndDelete(reqParams.orderId).then(result => {
		console.log(result)
		return  result.save().then(()=> {
			return result
		})
	}).catch((err) => {
		return err
	})
}

//archive Order
module.exports.archiveOrder = (reqParams) => {

	return Order.findByIdAndUpdate(reqParams.orderId).then(result => {
		if(result.status === true){

			result.status = false
			return result.save().then(()=> {
				return result
			}).catch(()=> {
				return false
			}) 
		} else { 
			return result
		}
	})

}

//Remove Item from cart 

module.exports.removeCartItem = async (userData, reqParams, reqBody) => {

	let {productId, totalAmount, status, subTotal, cartList} = reqBody

	let {orderId} = reqParams
	//quantity = Number.parseInt(reqBody.quantity)
	// console.log(orderId)
	try{

		let order = await Order.findOne({userId: userData.id, status: true});

		// console.log(productId)
		// console.log(order);


		if(order && order.status === true){

			let cartListIndex = order.cartList.findIndex(p => p.productId == productId);

			// console.log(cartListIndex);

			if(cartListIndex > -1){

				order.cartList.splice(cartListIndex, 1)
				order.totalAmount = order.cartList.reduce((a, {subTotal})=> a + subTotal,0)
				// console.log(order)
				return await order.save().then(() => {
					// console.log(order)
					return order
				}).catch((err) => {
					return err
				})

			}
		} else {
			return false
		} 
		
	}catch (err) {
		console.log(err)
		return err
	} 
}

// Edit CartItem Qty

module.exports.editCartItem = async (userData, reqParams, reqBody) => {
	
	let {productId, totalAmount, status, subTotal, cartList, quantity} = reqBody
	console.log(reqBody)
	let {orderId} = reqParams

	try{
		let order = await Order.findOne({userId: userData.id, status: true});

		const productDetails = await Product.findById(productId)
		//console.log(productId)
		if(order && order.status === true){

			let cartListIndex = order.cartList.findIndex(p => p.productId == productId);
			//console.log(cartListIndex)
			if(cartListIndex > -1){

				let orderItem = order.cartList[cartListIndex];
				console.log(reqBody.quantity)
				orderItem.quantity = quantity
				orderItem.subTotal = orderItem.quantity * productDetails.price
				order.totalAmount = order.cartList.reduce((a, {subTotal})=> a + subTotal,0) 

				order.cartList[cartListIndex] = orderItem;
			

				 return await order.save().then(() => {
				 	//console.log(productDetails.inStock)
				 	return order
				 }).catch((err) => {
				 	return err
				 })

			} else {
				return false
			}
		} else {
			return false
		}

	}catch (err) {
		console.log(err)
		return err
}
}


